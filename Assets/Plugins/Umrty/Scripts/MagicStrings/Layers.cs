// GENERATED //

using UnityEngine;

namespace Umrty.MagicStrings
{
    public class Layers
    {
        public static readonly string Default = "Default";
        public static readonly string TransparentFX = "TransparentFX";
        public static readonly string IgnoreRaycast = "Ignore Raycast";
        public static readonly string Water = "Water";
        public static readonly string UI = "UI";
        public static readonly string PostProcessing = "PostProcessing";

        public static readonly int DefaultInt = LayerMask.NameToLayer(Default);
        public static readonly int TransparentFXInt = LayerMask.NameToLayer(TransparentFX);
        public static readonly int IgnoreRaycastInt = LayerMask.NameToLayer(IgnoreRaycast);
        public static readonly int WaterInt = LayerMask.NameToLayer(Water);
        public static readonly int UIInt = LayerMask.NameToLayer(UI);
        public static readonly int PostProcessingInt = LayerMask.NameToLayer(PostProcessing);

        public static readonly int DefaultMask = 1 << DefaultInt;
        public static readonly int TransparentFXMask = 1 << TransparentFXInt;
        public static readonly int IgnoreRaycastMask = 1 << IgnoreRaycastInt;
        public static readonly int WaterMask = 1 << WaterInt;
        public static readonly int UIMask = 1 << UIInt;
        public static readonly int PostProcessingMask = 1 << PostProcessingInt;
    }
}
