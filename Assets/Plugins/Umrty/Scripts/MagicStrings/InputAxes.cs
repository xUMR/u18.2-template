// GENERATED //

namespace Umrty.MagicStrings
{
    public class InputAxes
    {
        public static readonly string Horizontal = "Horizontal";
        public static readonly string Vertical = "Vertical";
        public static readonly string Fire1 = "Fire1";
        public static readonly string Fire2 = "Fire2";
        public static readonly string Fire3 = "Fire3";
        public static readonly string Jump = "Jump";
        public static readonly string MouseX = "Mouse X";
        public static readonly string MouseY = "Mouse Y";
        public static readonly string MouseScrollWheel = "Mouse ScrollWheel";
        public static readonly string Submit = "Submit";
        public static readonly string Cancel = "Cancel";
    }
}
