// GENERATED //

using UnityEngine;

namespace Umrty.MagicStrings
{
    public class SortingLayers
    {
        public static readonly string Default = "Default";

        public static readonly int DefaultId = SortingLayer.NameToID(Default);

        public static readonly int DefaultValue = SortingLayer.GetLayerValueFromID(DefaultId);
    }
}
