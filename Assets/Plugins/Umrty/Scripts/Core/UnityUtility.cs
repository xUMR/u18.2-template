using System;
using System.Reflection;
using Umrty.Core.Extensions;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.SceneManagement;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core
{
    public static class UnityUtility
    {
        public static readonly object Object = new object();

        public static readonly Func<Component, Component, int> SortByPositionXAscYAsc =
            (customer1, customer2) =>
            {
                var position1 = customer1.transform.position;
                var position2 = customer2.transform.position;

                return position1.x.Approx(position2.x)
                    ? position1.y.CompareTo(position2.y)
                    : position1.x.CompareTo(position2.x);
            };

        public static readonly Func<Component, Component, int> SortByPositionXAscYDesc =
            (customer1, customer2) =>
            {
                var position1 = customer1.transform.position;
                var position2 = customer2.transform.position;

                return position1.x.Approx(position2.x)
                    ? position2.y.CompareTo(position1.y)
                    : position1.x.CompareTo(position2.x);
            };

        public static void RestartScene() => SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        public static void DestroyOtherInstances<T>(ref T instance) where T : MonoBehaviour
        {
            var objects = UnityEngine.Object.FindObjectsOfType<T>();
            foreach (var o in objects)
                if (instance != o)
                    UnityEngine.Object.Destroy(o.gameObject);
        }

        public static ref T EnsureSingleInstance<T>(ref T instance) where T : MonoBehaviour
        {
            if (instance != null)
                DestroyOtherInstances(ref instance);

            var objects = UnityEngine.Object.FindObjectsOfType<T>();
            if (objects.Length == 0)
            {
                var singleton = new GameObject(typeof(T).ToString());
                instance = singleton.AddComponent<T>();
            }
            else
            {
                instance = objects[0];
                for (var i = 1; i < objects.Length; i++)
                    UnityEngine.Object.Destroy(objects[i].gameObject);
            }

            return ref instance;
        }

        #region Editor

        public static void ClearConsole()
        {
#if UNITY_EDITOR
            Assembly.GetAssembly(typeof(UnityEditor.Editor))
                ?.GetType("UnityEditor.LogEntries")
                ?.GetMethod("Clear")
                ?.Invoke(Object, null);
#endif
        }

        public static void Pause()
        {
#if UNITY_EDITOR
            EditorApplication.isPaused = true;
#endif
        }

        public static void Unpause()
        {
#if UNITY_EDITOR
            EditorApplication.isPaused = false;
#endif
        }

        public static void PauseToggle()
        {
#if UNITY_EDITOR
            EditorApplication.isPaused = !EditorApplication.isPaused;
#endif
        }

        public static class Execute
        {
            public static void OnlyInEditor(Action action)
            {
#if UNITY_EDITOR
                action();
#endif
            }

            public static void OnlyInEditorWithState<TState>(TState state, Action<TState> action)
            {
#if UNITY_EDITOR
                action(state);
#endif
            }

            public static void OnlyInEditorWithState<TState1, TState2>(
                TState1 state1,
                TState2 state2,
                Action<TState1, TState2> action)
            {
#if UNITY_EDITOR
                action(state1, state2);
#endif
            }

            public static void OnlyInEditorWithState<TState1, TState2, TState3>(
                TState1 state1,
                TState2 state2,
                TState3 state3,
                Action<TState1, TState2, TState3> action)
            {
#if UNITY_EDITOR
                action(state1, state2, state3);
#endif
            }
        }

        #endregion
    }
}
