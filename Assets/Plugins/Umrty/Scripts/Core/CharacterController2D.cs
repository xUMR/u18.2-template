using Umrty.Core.Extensions;
using Umrty.MagicStrings;
using UniRx;
using UnityEngine;

namespace Umrty.Core
{
    public class CharacterController2D : MonoBehaviour
    {
        public float Speed;

        private Rigidbody2D _body;

        private void Start()
        {
            _body = GetComponentInChildren<Rigidbody2D>();

            if (_body == null || _body.isKinematic)
                TransformMovement();
            else
                RigidbodyMovement();
        }

        private void TransformMovement() =>
            InputManager.Axis2DStream[InputAxes.Horizontal, InputAxes.Vertical]
                .Where(v => !v.Approx(Vector2.zero))
                .Select(v => v.normalized * Time.deltaTime)
                .SubscribeWithState(this, (v, self) => self.transform.Translate(v * self.Speed));

        private void RigidbodyMovement() =>
            InputManager.Axis2DFixedStream[InputAxes.Horizontal, InputAxes.Vertical]
                .Select(v => v.normalized)
                .SubscribeWithState(this, (v, self) => self._body.velocity = v * self.Speed);
    }
}
