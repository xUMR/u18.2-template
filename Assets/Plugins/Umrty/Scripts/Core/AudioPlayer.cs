using System;
using System.Collections;
using Umrty.Core.Extensions;
using UniRx.Toolkit;
using UnityEngine;
using UnityEngine.Audio;

namespace Umrty.Core
{
    public class AudioPlayer : MonoBehaviour
    {
        [SerializeField] private AudioClip _audioClip;
        [SerializeField] private AudioSource _audioSource;
        public AudioMixerGroup Mixer;

        public ObjectPool<AudioPlayer> Pool;

        public bool Loop;

#if UNITY_EDITOR
        [NaughtyAttributes.ShowIf("Loop")] [NaughtyAttributes.MinMaxSlider(0, 120)]
#endif
        public Vector2 LoopStartEnd;

        public bool RandomizePitch;

#if UNITY_EDITOR
        [NaughtyAttributes.ShowIf("RandomizePitch")] [NaughtyAttributes.MinMaxSlider(-.5f, .5f)]
#endif
        public Vector2 PitchRandomRange;

        public float Length => Clip.length;

        public bool IsPlaying => _audioSource.isPlaying;

        public float AudioTime
        {
            get => _audioSource.time;
            set => _audioSource.time = value;
        }

        public AudioClip Clip
        {
            get => _audioClip;
            set => _audioClip = value;
        }

        public void Play()
        {
            _audioSource = this.GetOrAddComponent<AudioSource>();
            _audioSource.clip = Clip;
            _audioSource.outputAudioMixerGroup = Mixer;

            _audioSource.pitch = 1 + PitchRandomRange.Random();

            name = Loop
                ? $"{nameof(AudioPlayer)} ({Clip.name}) loop"
                : $"{nameof(AudioPlayer)} ({Clip.name})";

            _audioSource.Play();

            if (!Loop)
            {
                StartCoroutine(ReturnAfterPlaybackCoroutine(Clip.length + .1f));
            }
        }

        public AudioPlayer Stop()
        {
            _audioSource.Stop();
            return this;
        }

        public AudioPlayer Pause()
        {
            _audioSource.Pause();
            return this;
        }

        public AudioPlayer UnPause()
        {
            _audioSource.UnPause();
            return this;
        }

        public void Return() => Pool.Return(this);

        private IEnumerator ReturnAfterPlaybackCoroutine(float delay)
        {
            var waitUntil = Time.time + delay;
            while (Time.time < waitUntil)
                yield return null;

            Pool.Return(this);
        }

        private void OnEnable()
        {
            Play();
        }

        private void Update()
        {
            if (!Loop) return;

            if (AudioTime > LoopStartEnd.y)
                AudioTime = LoopStartEnd.x;
        }

        public void PitchOverTime(float target, float duration) =>
            StartCoroutine(
                LerpCoroutine(
                    audioSource => audioSource.pitch,
                    (audioSource, value) => audioSource.pitch = value,
                    target,
                    duration));

        public void VolumeOverTime(float target, float duration) =>
            StartCoroutine(
                LerpCoroutine(
                    audioSource => audioSource.volume,
                    (audioSource, value) => audioSource.volume = value,
                    target,
                    duration));

        public void PanOverTime(float target, float duration) =>
            StartCoroutine(
                LerpCoroutine(
                    audioSource => audioSource.panStereo,
                    (audioSource, value) => audioSource.panStereo = value,
                    target,
                    duration));

        private IEnumerator LerpCoroutine(
            Func<AudioSource, float> getter,
            Action<AudioSource, float> setter,
            float target,
            float duration)
        {
            var step = (target - getter(_audioSource)) / duration;

            while (getter(_audioSource) < target)
            {
                setter(_audioSource, getter(_audioSource) + step);
                yield return null;
            }

            setter(_audioSource, target);
        }
    }
}
