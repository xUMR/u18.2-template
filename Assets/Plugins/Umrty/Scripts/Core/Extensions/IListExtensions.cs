using System.Collections.Generic;
using UnityEngine;

namespace Umrty.Core.Extensions
{
    public static class IListExtensions
    {
        public static void Shuffle<T>(this IList<T> source)
        {
            for (var i = 0; i < source.Count - 1; i++)
            {
                var j = Random.Range(i, source.Count);
                source.Swap(i, j);
            }
        }

        public static void Swap<T>(this IList<T> source, int i, int j)
        {
            var temp = source[i];
            source[i] = source[j];
            source[i] = temp;
        }
    }
}
