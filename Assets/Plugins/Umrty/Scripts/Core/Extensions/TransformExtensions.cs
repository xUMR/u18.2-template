using System.Collections.Generic;
using UnityEngine;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core.Extensions
{
    public static class TransformExtensions
    {
        public static Transform GetFirstChild(this Transform self) => self.GetChild(0);
        public static Transform GetLastChild(this Transform self) => self.GetChild(self.childCount - 1);

        public static Transform[] GetChildren(this Transform self, Transform[] children = null)
        {
            children = children ?? new Transform[self.childCount];
            for (var i = 0; i < children.Length; i++)
                children[i] = self.GetChild(i);
            return children;
        }

        public static IEnumerable<Transform> GetChildrenEnumerable(this Transform self)
        {
            for (var i = 0; i < self.childCount; i++)
                yield return self.GetChild(i);
        }

        public static GameObject[] GetChildrenGameObjects(this Transform self, GameObject[] children = null)
        {
            children = children ?? new GameObject[self.childCount];
            for (var i = 0; i < children.Length; i++)
                children[i] = self.GetChild(i).gameObject;
            return children;
        }

        public static IEnumerable<GameObject> GetChildrenGameObjectsEnumerable(this Transform self)
        {
            for (var i = 0; i < self.childCount; i++)
                yield return self.GetChild(i).gameObject;
        }
    }
}
