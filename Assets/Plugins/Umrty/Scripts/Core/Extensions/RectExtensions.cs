using UnityEngine;

namespace Umrty.Core.Extensions
{
    public static class RectExtensions
    {
        public static Rect Copy(this Rect r) => new Rect(r);

        #region add methods

        public static Rect AddX(this Rect r, float f) => new Rect(r.x + f, r.y, r.width, r.height);
        public static Rect AddY(this Rect r, float f) => new Rect(r.x, r.y + f, r.width, r.height);
        public static Rect AddWidth(this Rect r, float f) => new Rect(r.x, r.y, r.width + f, r.height);
        public static Rect AddHeight(this Rect r, float f) => new Rect(r.x, r.y, r.width, r.height + f);

        #endregion

        #region with methods

        public static Rect WithX(this Rect r, float f) => new Rect(f, r.y, r.width, r.height);
        public static Rect WithY(this Rect r, float f) => new Rect(r.x, f, r.width, r.height);
        public static Rect WithWidth(this Rect r, float f) => new Rect(r.x, r.y, f, r.height);
        public static Rect WithHeight(this Rect r, float f) => new Rect(r.x, r.y, r.width, f);

        #endregion
    }
}
