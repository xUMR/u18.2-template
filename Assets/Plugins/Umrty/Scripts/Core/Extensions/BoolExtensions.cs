// ReSharper disable UnusedMember.Global

namespace Umrty.Core.Extensions
{
    public static class BoolExtensions
    {
        public static int AsInt(this bool b, int whenTrue = 1, int whenFalse = 0) => b ? whenTrue : whenFalse;

        public static T Select<T>(this bool self, T whenTrue, T whenFalse) => self ? whenTrue : whenFalse;
    }
}
