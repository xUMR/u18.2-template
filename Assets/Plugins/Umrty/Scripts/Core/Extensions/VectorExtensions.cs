using UnityEngine;
using Rng = UnityEngine.Random;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core.Extensions
{
    public static class VectorExtensions
    {
        #region Vector2 methods

        /// <summary>
        /// Returns a random float between [v.x, v.y].
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static float Random(this Vector2 v) => Rng.Range(v.x, v.y);

        public static Vector2 WithX(this Vector2 v, float x) => new Vector2(x, v.y);
        public static Vector2 WithY(this Vector2 v, float y) => new Vector2(v.x, y);

        public static Vector2 AddX(this Vector2 v, float x) => new Vector2(v.x + x, v.y);
        public static Vector2 AddY(this Vector2 v, float y) => new Vector2(v.x, v.y + y);

        public static float DistanceX(this Vector2 v, Vector2 t) => Mathf.Abs(v.x - t.x);
        public static float DistanceY(this Vector2 v, Vector2 t) => Mathf.Abs(v.y - t.y);

        public static bool Approx(this Vector2 v, Vector2 t) => v.x.Approx(t.x) && v.y.Approx(t.y);
        public static bool ApproxX(this Vector2 v, Vector2 t) => v.x.Approx(t.x);
        public static bool ApproxY(this Vector2 v, Vector2 t) => v.y.Approx(t.y);

        public static Vector2 RotateAround(this Vector2 point, Vector2 pivot, float zAngle) =>
            (Quaternion.Euler(0, 0, zAngle) * (point - pivot)).XY() + pivot;

        public static Vector2 RotateAround(this Vector2 point, Vector2 pivot, Vector3 angles) =>
            (Quaternion.Euler(angles) * (point - pivot)).XY() + pivot;

        #endregion

        #region Vector3 methods

        public static Vector3 WithX(this Vector3 v, float x) => new Vector3(x, v.y, v.z);
        public static Vector3 WithY(this Vector3 v, float y) => new Vector3(v.x, y, v.z);
        public static Vector3 WithZ(this Vector3 v, float z) => new Vector3(v.x, v.y, z);

        public static Vector3 WithXY(this Vector3 v, float x, float y) => new Vector3(x, y, v.z);
        public static Vector3 WithYZ(this Vector3 v, float y, float z) => new Vector3(v.x, y, z);
        public static Vector3 WithXZ(this Vector3 v, float x, float z) => new Vector3(x, v.y, z);

        public static Vector3 AddX(this Vector3 v, float x) => new Vector3(v.x + x, v.y, v.z);
        public static Vector3 AddY(this Vector3 v, float y) => new Vector3(v.x, v.y + y, v.z);
        public static Vector3 AddZ(this Vector3 v, float z) => new Vector3(v.x, v.y, v.z + z);

        public static Vector2 XY(this Vector3 v, Vector2 offset) => new Vector2(v.x + offset.x, v.y + offset.y);
        public static Vector2 XY(this Vector3 v, float x = 0, float y = 0) => new Vector2(v.x + x, v.y + y);
        public static Vector2 XZ(this Vector3 v, float x = 0, float z = 0) => new Vector2(v.x + x, v.z + z);
        public static Vector2 YZ(this Vector3 v, float y = 0, float z = 0) => new Vector2(v.y + y, v.z + z);

        public static float DistanceX(this Vector3 v, Vector3 t) => Mathf.Abs(v.x - t.x);
        public static float DistanceY(this Vector3 v, Vector3 t) => Mathf.Abs(v.y - t.y);
        public static float DistanceZ(this Vector3 v, Vector3 t) => Mathf.Abs(v.z - t.z);

        public static bool Approx(this Vector3 v, Vector3 t) => v.x.Approx(t.x) && v.y.Approx(t.y) && v.z.Approx(t.z);
        public static bool ApproxX(this Vector3 v, Vector3 t) => v.x.Approx(t.x);
        public static bool ApproxX(this Vector3 v, Vector2 t) => v.x.Approx(t.x);
        public static bool ApproxY(this Vector3 v, Vector3 t) => v.y.Approx(t.y);
        public static bool ApproxY(this Vector3 v, Vector2 t) => v.y.Approx(t.y);
        public static bool ApproxZ(this Vector3 v, Vector3 t) => v.z.Approx(t.z);
        public static bool ApproxXY(this Vector3 v, Vector3 t) => v.x.Approx(t.x) && v.y.Approx(t.y);
        public static bool ApproxXY(this Vector3 v, Vector2 t) => v.x.Approx(t.x) && v.y.Approx(t.y);

        public static Vector3 RotateAround(this Vector3 point, Vector3 pivot, float zAngle) =>
            Quaternion.Euler(0, 0, zAngle) * (point - pivot) + pivot;

        #endregion

        #region Color methods

        public static Color WithRed(this Color color, float r) => new Color(r, color.g, color.b, color.a);

        public static Color WithGreen(this Color color, float g) => new Color(color.r, g, color.b, color.a);

        public static Color WithBlue(this Color color, float b) => new Color(color.r, color.g, b, color.a);

        public static Color WithAlpha(this Color color, float a) => new Color(color.r, color.g, color.b, a);

        public static Color AddAlpha(this Color color, float a) => new Color(color.r, color.g, color.b, color.a + a);

        public static Color AddRgbOffset(this Color color, float offset) =>
            new Color(color.r + offset, color.g + offset, color.b + offset);

        public static Color AddRgb(this Color c, float r = 0, float g = 0, float b = 0, float a = 0) =>
            new Color(c.r + r, c.g + g, c.b + b, c.a + a);

        public static bool ApproxRgb(this Color c, Color t) =>
            Mathf.Approximately(c.r, t.r) && Mathf.Approximately(c.g, t.g) && Mathf.Approximately(c.b, t.b);

        public static bool ApproxRgba(this Color c, Color t) =>
            Mathf.Approximately(c.r, t.r) && Mathf.Approximately(c.g, t.g) && Mathf.Approximately(c.b, t.b) &&
            Mathf.Approximately(c.a, t.a);

        public static (byte r, byte g, byte b, byte a) ToHex(this Color c)
        {
            var r = (byte) (c.r * byte.MaxValue);
            var g = (byte) (c.g * byte.MaxValue);
            var b = (byte) (c.b * byte.MaxValue);
            var a = (byte) (c.a * byte.MaxValue);
            return (r, g, b, a);
        }

        public static string ToHexString(this Color c, string prefix = "")
        {
            var (r, g, b, a) = c.ToHex();
            return $"{prefix}{r:X2}{g:X2}{b:X2}{a:X2}";
        }

        #endregion

        public static bool Approx(this Quaternion q, Quaternion other) =>
            Quaternion.Angle(q, other) < Quaternion.kEpsilon;
    }
}
