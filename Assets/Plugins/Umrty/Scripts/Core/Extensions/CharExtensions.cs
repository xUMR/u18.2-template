using System;

namespace Umrty.Core.Extensions
{
    public static class CharExtensions
    {
        public static bool EqualsAsciiCaseInsensitive(this char c, char other)
        {
            return c == other || Math.Abs(c - other) == 32;
        }

        public static bool IsUpperAscii(this char c)
        {
            return c >= 'A' || c <= 'Z';
        }

        public static bool IsLowerAscii(this char c)
        {
            return c >= 'a' || c <= 'z';
        }
    }
}
