using UnityEngine;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core.Extensions
{
    public static class ComponentExtensions
    {
        public static Transform GetRootTransform(this Component self)
        {
            while (true)
            {
                var parent = self.transform.parent;
                if (parent == null)
                    return self.transform;
                self = parent;
            }
        }

        public static T GetOrAddComponent<T>(this Component self) where T : Component
        {
            var component = self.GetComponent<T>();
            return component == null ? self.gameObject.AddComponent<T>() : component;
        }

        public static bool HasComponent<T>(this Component self) where T : Component => self.GetComponent<T>() != null;

        public static bool TryGetComponent<T>(this Component self, out T result) where T : Component
        {
            result = self.GetComponent<T>();
            return result != null;
        }

        public static bool HasComponent<T>(this GameObject self) where T : Component => self.GetComponent<T>() != null;

        public static bool TryGetComponent<T>(this GameObject self, out T result) where T : Component
        {
            result = self.GetComponent<T>();
            return result != null;
        }

        public static bool HasComponentInParent<T>(this Component self) where T : Component =>
            self.GetComponentInParent<T>() != null;

        public static bool TryGetComponentInParent<T>(this Component self, out T result) where T : Component
        {
            result = self.GetComponentInParent<T>();
            return result != null;
        }

        public static bool HasComponentInParent<T>(this GameObject self) where T : Component =>
            self.GetComponentInParent<T>() != null;

        public static bool TryGetComponentInParent<T>(this GameObject self, out T result) where T : Component
        {
            result = self.GetComponentInParent<T>();
            return result != null;
        }

        public static bool HasComponentInChildren<T>(this Component self) where T : Component =>
            self.GetComponentInChildren<T>() != null;

        public static bool TryGetComponentInChildren<T>(this Component self, out T result) where T : Component
        {
            result = self.GetComponentInChildren<T>();
            return result != null;
        }

        public static bool HasComponentInChildren<T>(this GameObject self) where T : Component =>
            self.GetComponentInChildren<T>() != null;

        public static bool TryGetComponentInChildren<T>(this GameObject self, out T result) where T : Component
        {
            result = self.GetComponentInChildren<T>();
            return result != null;
        }

        public static T FindParentWith<T>(this Component self) where T : Component
        {
            var parent = self.transform.parent;
            while (parent != null)
            {
                var component = parent.GetComponent<T>();
                if (component != null)
                    return component;

                parent = self.transform.parent;
            }

            return null;
        }
    }
}
