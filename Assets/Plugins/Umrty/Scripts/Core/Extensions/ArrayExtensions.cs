using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = System.Random;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core.Extensions
{
    public static class ArrayExtensions
    {
        #region Median

        public static float Median(this int[] array)
        {
            var length = array.Length;
            var mid = length / 2;
            return length.IsOdd()
                ? array[mid]
                : (array[mid] + array[mid - 1]) / 2;
        }

        public static float Median(this float[] array)
        {
            var length = array.Length;
            var mid = length / 2;
            return length.IsOdd()
                ? array[mid]
                : (array[mid] + array[mid - 1]) / 2;
        }

        public static double Median(this double[] array)
        {
            var length = array.Length;
            var mid = length / 2;
            return length.IsOdd()
                ? array[mid]
                : (array[mid] + array[mid - 1]) / 2;
        }

        #endregion

        public static T Random<T>(this T[] array)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array));
            if (array.Length == 0)
                throw new ArgumentException("Collection must not be empty.");

            return array.ElementAt(UnityEngine.Random.Range(0, array.Length));
        }

        public static T Random<T>(this T[] array, Random random)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array));
            if (random == null)
                throw new ArgumentNullException(nameof(random));
            if (array.Length == 0)
                throw new ArgumentException("Collection must not be empty.");

            return array[random.Next(array.Length)];
        }

        public static bool Contains<T>(this T[] array, T t)
        {
            for (var i = array.Length - 1; i >= 0; i--)
                if (array[i].Equals(t))
                    return true;

            return false;
        }

        public static string AsString<T>(this T[] array)
        {
            var sb = new StringBuilder("[");

            for (var i = 0; i < array.Length - 1; i++)
                sb.Append($"{array[i]}, ");

            sb.Append($"{array[array.Length - 1]}]");

            return sb.ToString();
        }

        /// <summary>
        /// Shuffles the array and returns itself for convenience.
        /// </summary>
        /// <param name="self"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T[] Shuffle<T>(this T[] self)
        {
            var len = self.Length;
            for (var i = 0; i < len - 1; i++)
            {
                var j = UnityEngine.Random.Range(i, len);
                self.Swap(i, j);
            }

            return self;
        }

        public static void Swap<T>(this T[] self, int i0, int i1)
        {
            var temp = self[i0];
            self[i0] = self[i1];
            self[i1] = temp;
        }

        public static T Last<T>(this T[] self)
        {
            return self[self.Length - 1];
        }

        public static IEnumerable<T> GetNeighborsWrap<T>(this T[,] array, int row, int col)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array));

            var yLen = array.GetUpperBound(0) + 1;
            var xLen = array.GetUpperBound(1) + 1;

            // todo test & fix for arrays with sizes 1x1, 1x3, 3x1, 2x2, 2x3, 2x4, 3x2, 4x2

            for (var i = -1; i < 2; i++)
            {
                for (var j = -1; j < 2; j++)
                {
                    var y = (row + i + yLen) % yLen;
                    var x = (col + j + xLen) % xLen;

                    if (y != row || x != col) // skip the given element
                        yield return array[y, x];
                }
            }
        }

        public static IEnumerable<T> GetNeighbors<T>(this T[,] array, int row, int col, bool wrap = false)
        {
            if (wrap)
            {
                foreach (var item in array.GetNeighborsWrap(row, col))
                    yield return item;

                yield break;
            }

            if (array == null)
                throw new ArgumentNullException(nameof(array));

            var iMax = array.GetUpperBound(0);
            var jMax = array.GetUpperBound(1);

            // todo test & fix for arrays with sizes 1x1, 1x3, 3x1, 2x3, 2x4, 3x2, 4x2

            var iTop = Mathf.Clamp(row + 1, row, iMax);
            var jTop = Mathf.Clamp(col + 1, col, jMax);

            for (var i = Mathf.Clamp(row - 1, 0, iMax); i <= iTop; i++)
            for (var j = Mathf.Clamp(col - 1, 0, jMax); j <= jTop; j++)
                if (i != row || j != col) // skip the specified element
                    yield return array[i, j];
        }
    }
}
