using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// ReSharper disable UnusedMember.Global
namespace Umrty.Core.Extensions
{
    public static class IEnumerableExtensions
    {
        public static T Random<T>(this IEnumerable<T> source, Random random)
        {
            // https://youtu.be/H2KkiRbDZyc?t=2562
            var current = default(T);
            var count = 1.0;

            foreach (var item in source)
            {
                if (random.NextDouble() < 1 / count)
                {
                    current = item;
                }

                count++;
            }

            return current;
        }

        public static T Random<T>(this IEnumerable<T> source)
        {
            var current = default(T);
            var count = 1.0;

            foreach (var item in source)
            {
                if (UnityEngine.Random.value <= 1 / count)
                {
                    current = item;
                }

                count++;
            }

            return current;
        }

        public static T Random<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            var current = default(T);
            var count = 1.0;

            foreach (var item in source)
            {
                if (!predicate(item))
                    continue;

                if (UnityEngine.Random.value <= 1 / count)
                {
                    current = item;
                }

                count++;
            }

            return current;
        }

        public static T Next<T>(this IEnumerator<T> e)
        {
            e.MoveNext();
            return e.Current;
        }

        public static string AsString<T>(this IEnumerable<T> source,
            string prefix = "[",
            string suffix = "]",
            string separator = ", ")
        {
            var sb = new StringBuilder();
            sb.Append(prefix);
            foreach (var item in source)
            {
                sb.Append(item);
                sb.Append(separator);
            }

            sb.Remove(sb.Length - separator.Length, separator.Length);
            sb.Append(suffix);

            return sb.ToString();
        }

        #region AnyWithState

        public static bool AnyWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state))
                    return true;
            return false;
        }

        public static bool AnyWithState<TSource, TState1, TState2>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state1, state2))
                    return true;
            return false;
        }

        public static bool AnyWithState<TSource, TState1, TState2, TState3>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state1, state2, state3))
                    return true;
            return false;
        }

        #endregion

        #region AllWithState

        public static bool AllWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (!predicate(item, state))
                    return false;
            return true;
        }

        public static bool AllWithState<TSource, TState1, TState2>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (!predicate(item, state1, state2))
                    return false;
            return true;
        }

        public static bool AllWithState<TSource, TState1, TState2, TState3>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (!predicate(item, state1, state2, state3))
                    return false;
            return true;
        }

        #endregion

        #region ForEachWithState

        public static void ForEachWithState<T, TState>(
            this IEnumerable<T> source,
            TState state,
            Action<T, TState> action)
        {
            foreach (var t in source)
                action(t, state);
        }

        public static void ForEachWithState<T, TState1, TState2>(
            this IEnumerable<T> source,
            TState1 state1,
            TState2 state2,
            Action<T, TState1, TState2> action)
        {
            foreach (var t in source)
                action(t, state1, state2);
        }

        public static void ForEachWithState<T, TState1, TState2, TState3>(
            this IEnumerable<T> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Action<T, TState1, TState2, TState3> action)
        {
            foreach (var t in source)
                action(t, state1, state2, state3);
        }

        #endregion

        #region ForFirst & WithState

        public static void ForFirst<T>(this IEnumerable<T> source, int count, Action<T> action)
        {
            if (count < 1) return;

            var i = 0;
            foreach (var item in source)
            {
                if (i >= count)
                    return;

                action(item);

                i++;
            }
        }

        public static void ForFirstWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            int count,
            Action<TSource, TState> action)
        {
            if (count < 1) return;

            var i = 0;
            foreach (var item in source)
            {
                if (i >= count)
                    return;

                action(item, state);

                i++;
            }
        }

        #endregion

        #region FirstWithState

        public static TSource FirstWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state))
                    return item;

            throw new InvalidOperationException();
        }

        public static TSource FirstWithState<TSource, TState1, TState2>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state1, state2))
                    return item;

            throw new InvalidOperationException();
        }

        public static TSource FirstWithState<TSource, TState1, TState2, TState3>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state1, state2, state3))
                    return item;

            throw new InvalidOperationException();
        }

        #endregion

        #region FirstOrDefaultWithState

        public static TSource FirstOrDefaultWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state))
                    return item;

            return default(TSource);
        }

        public static TSource FirstOrDefaultWithState<TSource, TState1, TState2>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state1, state2))
                    return item;

            return default(TSource);
        }

        public static TSource FirstOrDefaultWithState<TSource, TState1, TState2, TState3>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state1, state2, state3))
                    return item;

            return default(TSource);
        }

        #endregion

        #region FirstAndIndex & WithState

        public static (T item, int index) FirstAndIndex<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var i = 0;
            foreach (var e in source)
            {
                if (predicate(e))
                    return (e, i);
                i++;
            }

            throw new InvalidOperationException();
        }

        public static (TSource item, int index) FirstAndIndexWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var i = 0;
            foreach (var e in source)
            {
                if (predicate(e, state))
                    return (e, i);
                i++;
            }

            throw new InvalidOperationException();
        }

        #endregion

        #region FirstAndIndexOrDefault & WithState

        public static (T item, int index) FirstAndIndexOrDefault<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var i = 0;
            foreach (var e in source)
            {
                if (predicate(e))
                    return (e, i);
                i++;
            }

            return (default(T), -1);
        }

        public static (TSource item, int index) FirstAndIndexOrDefaultWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var i = 0;
            foreach (var e in source)
            {
                if (predicate(e, state))
                    return (e, i);
                i++;
            }

            return (default(TSource), -1);
        }

        #endregion

        #region LastWithState

        public static TSource LastWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var lastItem = default(TSource);
            var isFound = false;
            foreach (var item in source)
            {
                if (predicate(item, state))
                {
                    lastItem = item;
                    isFound = true;
                }
            }

            if (isFound)
                return lastItem;

            throw new InvalidOperationException();
        }

        public static TSource LastWithState<TSource, TState1, TState2>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var lastItem = default(TSource);
            var isFound = false;
            foreach (var item in source)
            {
                if (predicate(item, state1, state2))
                {
                    lastItem = item;
                    isFound = true;
                }
            }

            if (isFound)
                return lastItem;

            throw new InvalidOperationException();
        }

        public static TSource LastWithState<TSource, TState1, TState2, TState3>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var lastItem = default(TSource);
            var isFound = false;
            foreach (var item in source)
            {
                if (predicate(item, state1, state2, state3))
                {
                    lastItem = item;
                    isFound = true;
                }
            }

            if (isFound)
                return lastItem;

            throw new InvalidOperationException();
        }

        #endregion

        #region LastOrDefaultWithState

        public static TSource LastOrDefaultWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var lastItem = default(TSource);
            var isFound = false;
            foreach (var item in source)
            {
                if (predicate(item, state))
                {
                    lastItem = item;
                    isFound = true;
                }
            }

            return isFound ? lastItem : default(TSource);
        }

        public static TSource LastOrDefaultWithState<TSource, TState1, TState2>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var lastItem = default(TSource);
            var isFound = false;
            foreach (var item in source)
            {
                if (predicate(item, state1, state2))
                {
                    lastItem = item;
                    isFound = true;
                }
            }

            return isFound ? lastItem : default(TSource);
        }

        public static TSource LastOrDefaultWithState<TSource, TState1, TState2, TState3>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var lastItem = default(TSource);
            var isFound = false;
            foreach (var item in source)
            {
                if (predicate(item, state1, state2, state3))
                {
                    lastItem = item;
                    isFound = true;
                }
            }

            return isFound ? lastItem : default(TSource);
        }

        #endregion

        #region SingleWithState

        public static TSource SingleWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var count = 0;
            TSource singleItem = default(TSource);
            foreach (var item in source)
            {
                if (predicate(item, state))
                {
                    singleItem = item;
                    count++;
                }
            }

            if (count == 0 || count > 1)
                throw new InvalidOperationException();

            return singleItem;
        }

        public static TSource SingleWithState<TSource, TState1, TState2>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var count = 0;
            TSource singleItem = default(TSource);
            foreach (var item in source)
            {
                if (predicate(item, state1, state2))
                {
                    singleItem = item;
                    count++;
                }
            }

            if (count == 0 || count > 1)
                throw new InvalidOperationException();

            return singleItem;
        }

        public static TSource SingleWithState<TSource, TState1, TState2, TState3>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var count = 0;
            TSource singleItem = default(TSource);
            foreach (var item in source)
            {
                if (predicate(item, state1, state2, state3))
                {
                    singleItem = item;
                    count++;
                }
            }

            if (count == 0 || count > 1)
                throw new InvalidOperationException();

            return singleItem;
        }

        #endregion

        #region SingleOrDefaultWithState

        public static TSource SingleOrDefaultWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var count = 0;
            TSource singleItem = default(TSource);
            foreach (var item in source)
            {
                if (predicate(item, state))
                {
                    singleItem = item;
                    count++;
                }
            }

            if (count < 2)
                return singleItem;

            throw new InvalidOperationException();
        }

        public static TSource SingleOrDefaultWithState<TSource, TState1, TState2>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var count = 0;
            TSource singleItem = default(TSource);
            foreach (var item in source)
            {
                if (predicate(item, state1, state2))
                {
                    singleItem = item;
                    count++;
                }
            }

            if (count < 2)
                return singleItem;

            throw new InvalidOperationException();
        }

        public static TSource SingleOrDefaultWithState<TSource, TState1, TState2, TState3>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var count = 0;
            TSource singleItem = default(TSource);
            foreach (var item in source)
            {
                if (predicate(item, state1, state2, state3))
                {
                    singleItem = item;
                    count++;
                }
            }

            if (count < 2)
                return singleItem;

            throw new InvalidOperationException();
        }

        #endregion

        #region SelectWithState

        public static IEnumerable<TResult> SelectWithState<TSource, TState, TResult>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, TResult> selector)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (selector == null)
                throw new ArgumentNullException(nameof(selector));

            foreach (var item in source)
                yield return selector(item, state);
        }

        public static IEnumerable<TResult> SelectWithState<TSource, TState1, TState2, TResult>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, TResult> selector)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (selector == null)
                throw new ArgumentNullException(nameof(selector));

            foreach (var item in source)
                yield return selector(item, state1, state2);
        }

        public static IEnumerable<TResult> SelectWithState<TSource, TState1, TState2, TState3, TResult>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, TResult> selector)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (selector == null)
                throw new ArgumentNullException(nameof(selector));

            foreach (var item in source)
                yield return selector(item, state1, state2, state3);
        }

        public static IEnumerable<TResult> SelectWithState<TSource, TState, TResult>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, int, TResult> selector)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (selector == null)
                throw new ArgumentNullException(nameof(selector));

            var index = 0;
            foreach (var item in source)
            {
                yield return selector(item, state, index);
                index++;
            }
        }

        public static IEnumerable<TResult> SelectWithState<TSource, TState1, TState2, TResult>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, int, TResult> selector)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (selector == null)
                throw new ArgumentNullException(nameof(selector));

            var index = 0;
            foreach (var item in source)
            {
                yield return selector(item, state1, state2, index);
                index++;
            }
        }

        public static IEnumerable<TResult> SelectWithState<TSource, TState1, TState2, TState3, TResult>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, int, TResult> selector)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (selector == null)
                throw new ArgumentNullException(nameof(selector));

            var index = 0;
            foreach (var item in source)
            {
                yield return selector(item, state1, state2, state3, index);
                index++;
            }
        }

        #endregion

        #region WhereWithState

        public static IEnumerable<TSource> WhereWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state))
                    yield return item;
        }

        public static IEnumerable<TSource> WhereWithState<TSource, TState1, TState2>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state1, state2))
                    yield return item;
        }

        public static IEnumerable<TSource> WhereWithState<TSource, TState1, TState2, TState3>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            foreach (var item in source)
                if (predicate(item, state1, state2, state3))
                    yield return item;
        }

        public static IEnumerable<TSource> WhereWithState<TSource, TState>(
            this IEnumerable<TSource> source,
            TState state,
            Func<TSource, TState, int, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var index = 0;
            foreach (var item in source)
            {
                if (predicate(item, state, index))
                    yield return item;
                index++;
            }
        }

        public static IEnumerable<TSource> WhereWithState<TSource, TState1, TState2>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            Func<TSource, TState1, TState2, int, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var index = 0;
            foreach (var item in source)
            {
                if (predicate(item, state1, state2, index))
                    yield return item;
                index++;
            }
        }

        public static IEnumerable<TSource> WhereWithState<TSource, TState1, TState2, TState3>(
            this IEnumerable<TSource> source,
            TState1 state1,
            TState2 state2,
            TState3 state3,
            Func<TSource, TState1, TState2, TState3, int, bool> predicate)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var index = 0;
            foreach (var item in source)
            {
                if (predicate(item, state1, state2, state3, index))
                    yield return item;
                index++;
            }
        }

        #endregion
    }
}
