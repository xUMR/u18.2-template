using System.Text;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core.Extensions
{
    public static class StringBuilderExtensions
    {
        public static StringBuilder PadLeft(
            this StringBuilder stringBuilder,
            int totalWidth,
            char paddingChar = ' ')
        {
            for (var i = 0; i < totalWidth - stringBuilder.Length; i++)
                stringBuilder.Append(paddingChar);

            return stringBuilder;
        }

        public static StringBuilder PadRight(
            this StringBuilder stringBuilder,
            int totalWidth,
            char paddingChar = ' ')
        {
            for (var i = 0; i < totalWidth - stringBuilder.Length; i++)
                stringBuilder.Insert(0, paddingChar);

            return stringBuilder;
        }
    }
}
