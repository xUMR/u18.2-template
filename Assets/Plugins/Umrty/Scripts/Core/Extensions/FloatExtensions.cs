using System;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core.Extensions
{
    public static class FloatExtensions
    {
        public static bool IsNonPositive(this float f) => f < float.Epsilon;
        public static bool IsNonNegative(this float f) => f > -float.Epsilon;
        public static bool IsZero(this float f) => Math.Abs(f) < float.Epsilon;
        public static bool IsZero(this float f, float epsilon) => Math.Abs(f) < epsilon;
        public static bool IsNonZero(this float f) => !IsZero(f);

        public static bool Approx(this float f, float other) => IsZero(f - other);
        public static bool Approx(this float f, float other, float epsilon) => IsZero(f - other, epsilon);
        public static bool LessOrApprox(this float f, float other) => Approx(f, other) || f < other;
        public static bool GreaterOrApprox(this float f, float other) => Approx(f, other) || f > other;

        public static long FloorToLong(this float f)
        {
            return Convert.ToInt64(Math.Floor(f));
        }

        public static bool IsInRange(this float i, float min, float max)
        {
            return i > min && i < max;
        }

        public static float Lerp(this float f, float x0, float x1) => (1 - f) * x0 + f * x1;

        public static float Smoothstep(this float f, float x0, float x1)
        {
            var t = Clamp((f - x0) / (x1 - x0));
            return t * t * (3 - 2 * t);
        }

        public static float Clamp(this float f, float min = 0, float max = 1)
        {
            if (f < min) return min;
            if (f > max) return max;
            return f;
        }

        public static T Select<T>(this float f, T whenNegative, T whenZero, T whenPositive)
        {
            if (IsZero(f)) return whenZero;
            return f > 0 ? whenPositive : whenNegative;
        }

        public static float RoundToNearestMultipleOf(this float f, float k)
        {
            var m = f % k;
            if (m < k / 2) return f - m;
            return f + k - m;
        }
    }
}
