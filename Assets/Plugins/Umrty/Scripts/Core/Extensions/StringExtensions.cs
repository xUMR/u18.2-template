using System.Linq;
using System.Text;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveWhiteSpace(this string self, StringBuilder sb = null)
        {
            sb = sb ?? new StringBuilder();
            foreach (var c in self)
                if (!char.IsWhiteSpace(c))
                    sb.Append(c);
            return sb.ToString();
        }

        public static string RemoveChars(this string self, string charsToRemove, StringBuilder sb = null)
        {
            sb = sb ?? new StringBuilder();
            foreach (var c in self)
                if (!charsToRemove.Contains(c))
                    sb.Append(c);
            return sb.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="self"></param>
        /// <param name="minLength"></param>
        /// <param name="maxLength">Inclusive</param>
        /// <param name="sb"></param>
        /// <returns></returns>
        public static string PickRandom(this string self, int minLength = 1, int maxLength = 1, StringBuilder sb = null)
        {
            sb = sb ?? new StringBuilder();

            var length = UnityEngine.Random.Range(minLength, maxLength + 1);
            for (var i = 0; i < length; i++)
                sb.Append(self[UnityEngine.Random.Range(0, self.Length)]);

            return sb.ToString();
        }

        public static int CountNonWhiteSpace(this string self) => self.Count(t => !char.IsWhiteSpace(t));

        public static string InsertEnd(this string self, int index, string str) =>
            self.Insert(self.Length - index, str);

        public static string Truncate(this string self, int chars) => self.Substring(0, self.Length - chars);

        public static string Shuffle(this string self) => new string(ArrayExtensions.Shuffle(self.ToCharArray()));
    }
}
