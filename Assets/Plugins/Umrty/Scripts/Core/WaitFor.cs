using System.Collections;
using UnityEngine;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core
{
    public static class WaitFor
    {
        private static WaitForSecondsUnscaled SecondsUnscaled1 { get; } = new WaitForSecondsUnscaled(1);
        private static WaitForSeconds Seconds1 { get; } = new WaitForSeconds(1);
        public static WaitForEndOfFrame EndOfFrame { get; } = new WaitForEndOfFrame();
        public static WaitForFixedUpdate FixedUpdate { get; } = new WaitForFixedUpdate();

        public static IEnumerator SecondsUnscaled(int seconds)
        {
            for (var i = seconds - 1; i >= 0; i--)
                yield return SecondsUnscaled1;
        }

        public static IEnumerator Seconds(int seconds)
        {
            for (var i = seconds - 1; i >= 0; i--)
                yield return Seconds1;
        }

        public static IEnumerator Frames(int frames)
        {
            for (var i = frames - 1; i >= 0; i--)
                yield return EndOfFrame;
        }
    }
}
