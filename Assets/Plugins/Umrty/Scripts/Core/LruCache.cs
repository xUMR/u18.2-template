using System.Collections.Generic;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core
{
    public class LruCache<TKey, TValue>
    {
        private readonly Dictionary<TKey, Entry<TKey, TValue>> _dictionary;
        private readonly int _capacity;
        private Entry<TKey, TValue> _head;
        private Entry<TKey, TValue> _tail;

        public LruCache(int capacity)
        {
            _dictionary = new Dictionary<TKey, Entry<TKey, TValue>>(capacity);
            _capacity = capacity;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="key"></param>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException"></exception>
        public TValue this[TKey key]
        {
            get => Get(key);
            set => Put(key, value);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="key"></param>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException"></exception>
        /// <returns></returns>
        public TValue Get(TKey key)
        {
            var entry = _dictionary[key];
            // KeyNotFoundException is expected
            // if an exception is not thrown
            MoveToTop(entry);
            return entry.Value;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            if (_dictionary.TryGetValue(key, out var entry))
            {
                value = entry.Value;
                MoveToTop(entry);
                return true;
            }

            value = default(TValue);
            return false;
        }

        public void Put(TKey key, TValue value)
        {
            if (_dictionary.TryGetValue(key, out var previousEntry))
            {
                MoveToTop(previousEntry);
                return;
            }

            EvictOldestIfFull();

            var entry = new Entry<TKey, TValue>(key, value);
            if (_head == null)
                _head = entry;
            else
                entry.Next = _head;

            if (_tail == null)
                _tail = entry;

            _dictionary[key] = entry;
            MoveToTop(entry);
        }

        private void MoveToTop(Entry<TKey, TValue> entry)
        {
            if (entry.Next != null)
                entry.Next.Prev = entry.Prev;
            if (entry.Prev != null)
                entry.Prev.Next = entry.Next;

            if (_tail == entry && _dictionary.Count > 1)
            {
                _tail = entry.Prev;
                _tail.Next = null;
            }

            _head.Prev = entry;
            entry.Next = _head;
            entry.Prev = null;

            _head = entry;
        }

        private void EvictOldestIfFull()
        {
            if (_dictionary.Count != _capacity) return;
            if (_tail == null) return;

            var keyToRemove = _tail.Key;
            _tail = _tail.Prev;
            _dictionary.Remove(keyToRemove);
        }

        public class Entry<TKey, TValue>
        {
            public TValue Value { get; }
            public TKey Key { get; }
            public Entry<TKey, TValue> Prev { get; internal set; }
            public Entry<TKey, TValue> Next { get; internal set; }

            public Entry(TKey key, TValue value)
            {
                Key = key;
                Value = value;
            }
        }
    }
}
