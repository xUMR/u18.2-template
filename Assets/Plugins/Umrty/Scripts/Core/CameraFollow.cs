using UnityEngine;

namespace Umrty.Core
{
    [RequireComponent(typeof(Camera))]
    public class CameraFollow : MonoBehaviour
    {
        public Transform Target;
        public Vector3 Offset;
        public float Speed;
        public bool LookAtTarget;

        private Vector3 _velocity;

        private void LateUpdate()
        {
            var destination = Target.position + Offset;
            var source = transform.position;
            transform.position = Vector3.SmoothDamp(source, destination, ref _velocity, Speed * Time.deltaTime);

            if (LookAtTarget)
                transform.LookAt(Target);
        }
    }
}
