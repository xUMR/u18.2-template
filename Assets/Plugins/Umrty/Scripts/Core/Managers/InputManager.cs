using System;
using System.Collections.Generic;
using Umrty.Core.Extensions;
using UniRx;
using UnityEngine;

//Usage examples:
//
//    var selector = InputManager.GetAxisSelector("Horizontal", "Vertical");
//
//    Observable.EveryUpdate()
//        .Select(selector)
//        .Where(v => !v.Approx(Vector2.zero))
//        .Select(v => v.normalized * Time.deltaTime)
//
//    // or
//
//    InputManager.Axis2DStream["Horizontal", "Vertical"]
//        .Where(v => !v.Approx(Vector2.zero))
//        .Select(v => v.normalized);
//
//    // ---
//
//    InputManager.OnInputString
//        .Where(s => s.Length > 0);
//
//    // ---
//
//    InputManager.KeyStream[KeyCode.Tab]
//        .Where(key => key.IsDown());
//

namespace Umrty.Core
{
    public class InputManager : MonoBehaviour
    {
        public static InputManager Instance;

        public static KeyStreamIndexer KeyStream;
        public static AxisStreamIndexer AxisStream;
        public static Axis2DStreamIndexer Axis2DStream;
        public static AxisFixedStreamIndexer AxisFixedStream;
        public static Axis2DFixedStreamIndexer Axis2DFixedStream;

        public static IObservable<string> OnInputStringUpperInvariant { get; private set; }
        public static IObservable<string> OnInputString { get; private set; }

        private Dictionary<KeyCode, Func<long, (bool isDown, bool isUp)>> _keyStateSelectors;
        private Dictionary<string, Func<long, float>> _axisSelectors;
        private Dictionary<(string, string), Func<long, Vector2>> _axis2DSelectors;

        private static KeyCode[] _keyCodes;

        private void Awake()
        {
            this.EnsureSingleInstance(ref Instance).DontDestroyOnLoad();

            _keyStateSelectors = new Dictionary<KeyCode, Func<long, (bool isDown, bool isUp)>>();
            _axisSelectors = new Dictionary<string, Func<long, float>>();
            _axis2DSelectors = new Dictionary<(string, string), Func<long, Vector2>>();

            _keyCodes = _keyCodes ?? (KeyCode[]) Enum.GetValues(typeof(KeyCode));

            KeyStream = new KeyStreamIndexer();
            AxisStream = new AxisStreamIndexer();
            Axis2DStream = new Axis2DStreamIndexer();
            AxisFixedStream = new AxisFixedStreamIndexer();
            Axis2DFixedStream = new Axis2DFixedStreamIndexer();

            OnInputStringUpperInvariant = OnInputStringUpperInvariantObservable();
            OnInputString = OnInputStringObservable();
        }

        public static Func<long, float> GetAxisSelector(string axis)
        {
            if (!Instance._axisSelectors.TryGetValue(axis, out var axisSelector))
            {
                axisSelector = _ => Input.GetAxisRaw(axis);
                Instance._axisSelectors[axis] = axisSelector;
            }

            return axisSelector;
        }

        public static Func<long, Vector2> GetAxisSelector(string axis1, string axis2)
        {
            var pair = (axis1, axis2);
            if (!Instance._axis2DSelectors.TryGetValue(pair, out var axis2DSelector))
            {
                axis2DSelector = _ => new Vector2(Input.GetAxisRaw(axis1), Input.GetAxisRaw(axis2));
                Instance._axis2DSelectors[pair] = axis2DSelector;
            }

            return axis2DSelector;
        }

        public static IObservable<float> OnAxisObservable(string axis) =>
            Observable.EveryUpdate().Select(GetAxisSelector(axis));

        public static IObservable<Vector2> OnAxisObservable(string axis1, string axis2) =>
            Observable.EveryUpdate().Select(GetAxisSelector(axis1, axis2));

        public static IObservable<float> OnAxisFixedObservable(string axis) =>
            Observable.EveryFixedUpdate().Select(GetAxisSelector(axis));

        public static IObservable<Vector2> OnAxisFixedObservable(string axis1, string axis2) =>
            Observable.EveryFixedUpdate().Select(GetAxisSelector(axis1, axis2));

        public static IObservable<KeyState> OnKeyObservable(KeyCode keyCode)
        {
            if (!Instance._keyStateSelectors.TryGetValue(keyCode, out var keyStateSelector))
            {
                keyStateSelector = _ =>
                {
                    var isDown = Input.GetKeyDown(keyCode);
                    var isUp = Input.GetKeyUp(keyCode);

                    // possible states:
                    // false, false
                    // true, false
                    // false, true
                    return (isDown, isUp);
                };

                Instance._keyStateSelectors[keyCode] = keyStateSelector;
            }

            return Observable.EveryUpdate()
                .Select(keyStateSelector)
                // disallow (false, false) since it's useless and it would repeat
                .Where(key => key.isUp || key.isDown)
                .Select(key => key.isDown && !key.isUp ? KeyState.IsDown : KeyState.IsUp);
//            .Select(key => key.isDown && !key.isUp); // true if isDown, false if isUp
        }

        private static IObservable<string> OnInputStringUpperInvariantObservable() =>
            Observable.EveryUpdate().Select(_ => Input.inputString.ToUpperInvariant());

        private static IObservable<string> OnInputStringObservable() =>
            Observable.EveryUpdate().Select(_ => Input.inputString);

        #region Indexers

        public sealed class KeyStreamIndexer
        {
            internal KeyStreamIndexer() { }

            public IObservable<KeyState> this[KeyCode keyCode] => OnKeyObservable(keyCode);
        }

        public sealed class AxisStreamIndexer
        {
            internal AxisStreamIndexer() { }

            public IObservable<float> this[string axis] => OnAxisObservable(axis);
        }

        public sealed class Axis2DStreamIndexer
        {
            internal Axis2DStreamIndexer() { }

            public IObservable<Vector2> this[string axis1, string axis2] => OnAxisObservable(axis1, axis2);
        }

        public sealed class AxisFixedStreamIndexer
        {
            internal AxisFixedStreamIndexer() { }

            public IObservable<float> this[string axis] => OnAxisFixedObservable(axis);
        }

        public sealed class Axis2DFixedStreamIndexer
        {
            internal Axis2DFixedStreamIndexer() { }

            public IObservable<Vector2> this[string axis1, string axis2] => OnAxisFixedObservable(axis1, axis2);
        }

        #endregion
    }

    public enum KeyState
    {
        IsDown,
        IsUp
    }

    public static class KeyStateExtensions
    {
        public static bool IsDown(this KeyState keyState) => keyState == KeyState.IsDown;
        public static bool IsUp(this KeyState keyState) => keyState == KeyState.IsUp;
    }
}
