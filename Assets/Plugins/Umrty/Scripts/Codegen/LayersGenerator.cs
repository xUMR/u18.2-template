using System;
using System.Collections.Generic;
using System.IO;
using Umrty.Core.Extensions;
using UnityEditor;

namespace Umrty.Codegen
{
    public class LayersGenerator : BaseGenerator
    {
        protected override IEnumerable<Func<string, string>> VariableGenerators { get; } =
            new List<Func<string, string>> {GenerateLayer, GenerateLayerInt, GenerateLayerMask};

        protected override IEnumerable<string> RequiredNamespaces { get; } =
            new List<string> {typeof(UnityEngine.LayerMask).Namespace};

        private static string GenerateLayer(string item) =>
            PublicStaticReadonlyVariable("string", Field(item), $"\"{item}\"");

        private static string GenerateLayerInt(string item)
        {
            var field = Field(item);
            var fieldInt = $"{field}Int";
            return PublicStaticReadonlyVariable("int", fieldInt, $"LayerMask.NameToLayer({field})");
        }

        private static string GenerateLayerMask(string item)
        {
            var field = Field(item);
            var fieldMask = $"{field}Mask";
            var fieldInt = $"{field}Int";
            return PublicStaticReadonlyVariable("int", fieldMask, $"1 << {fieldInt}");
        }

        protected override IEnumerable<string> ParseItems()
        {
            var obj = new SerializedObject(
                AssetDatabase.LoadAssetAtPath(Path.Combine(ProjectSettingsDirectory, "TagManager.asset"),
                    typeof(object)));

            var layers = obj.FindProperty("layers");
            for (var i = 0; i < layers.arraySize; i++)
            {
                var layer = layers.GetArrayElementAtIndex(i).stringValue;
                if (!string.IsNullOrWhiteSpace(layer))
                    yield return layer;
            }
        }
    }
}
