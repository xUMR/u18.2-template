using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Umrty.Core.Extensions;
using UnityEditor;

namespace Umrty.Codegen
{
    public class SortingLayersGenerator : BaseGenerator
    {
        protected override IEnumerable<Func<string, string>> VariableGenerators { get; } =
            new List<Func<string, string>> {GenerateSortingLayer, GenerateSortingLayerId, GenerateSortingLayerValue};

        protected override IEnumerable<string> RequiredNamespaces { get; } =
            new List<string> {typeof(UnityEngine.SortingLayer).Namespace};

        private static string GenerateSortingLayer(string item) =>
            PublicStaticReadonlyVariable("string", Field(item), $"\"{item}\"");

        private static string GenerateSortingLayerId(string item)
        {
            var field = Field(item);
            var fieldId = $"{field}Id";
            return PublicStaticReadonlyVariable("int", fieldId, $"SortingLayer.NameToID({field})");
        }

        private static string GenerateSortingLayerValue(string item)
        {
            var field = Field(item);
            var fieldId = $"{field}Id";
            var fieldValue = $"{field}Value";
            return PublicStaticReadonlyVariable("int", fieldValue, $"SortingLayer.GetLayerValueFromID({fieldId})");
        }

        protected override IEnumerable<string> ParseItems()
        {
            var obj = new SerializedObject(
                AssetDatabase.LoadAssetAtPath(Path.Combine(ProjectSettingsDirectory, "TagManager.asset"),
                    typeof(object)));

            var sortingLayers = obj.FindProperty("m_SortingLayers");
            for (var i = 0; i < sortingLayers.arraySize; i++)
            {
                var sortingLayer = sortingLayers.GetArrayElementAtIndex(i).FindPropertyRelative("name").stringValue;
                if (!string.IsNullOrWhiteSpace(sortingLayer))
                    yield return sortingLayer;
            }
        }
    }
}
