using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;

namespace Umrty.Codegen
{
    public class InputAxesGenerator : BaseGenerator
    {
        protected override IEnumerable<Func<string, string>> VariableGenerators { get; } =
            new List<Func<string, string>> {GenerateInputAxis};

        protected override IEnumerable<string> RequiredNamespaces { get; } = Enumerable.Empty<string>();

        private static string GenerateInputAxis(string item) =>
            PublicStaticReadonlyVariable("string", Field(item), $"\"{item}\"");

        protected override IEnumerable<string> ParseItems()
        {
            var obj = new SerializedObject(
                AssetDatabase.LoadAssetAtPath(Path.Combine(ProjectSettingsDirectory, "InputManager.asset"),
                    typeof(object)));

            var axes = obj.FindProperty("m_Axes");
            for (var i = 0; i < axes.arraySize; i++)
            {
                yield return axes.GetArrayElementAtIndex(i).FindPropertyRelative("m_Name").stringValue;
            }
        }
    }
}
