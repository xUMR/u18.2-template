using UnityEngine;

public class Greeter : MonoBehaviour
{
    public void Greet(string address)
    {
        print($"Hello, {address}!");
    }
}
