using UnityEngine;

public class GameManager : MonoBehaviour
{
    public AddressBook AddressBook;
    public Greeter Prefab;

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    public void Greet()
    {
        var go = Instantiate(Prefab);
        var address = AddressBook.Random();
        go.Greet(address);

        Destroy(go.gameObject, 1);
    }
}
