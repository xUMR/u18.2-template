using UnityEngine;
using Umrty.Core.Extensions;

[CreateAssetMenu(fileName = "AddressBook", menuName = "SO/AddressBook")]
public class AddressBook : ScriptableObject
{
    public string[] Addresses;

    public string this[int index]
    {
        get => Addresses[index];
        set => Addresses[index] = value;
    }

    public string Random() => Addresses.Random();
}
